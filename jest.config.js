module.exports = {
  coverageDirectory: 'coverage',
  testEnvironment: 'node',
  preset: 'react-native',
  setupFiles: ['./node_modules/react-native-gesture-handler/jestSetup.js'],
  transformIgnorePatterns: ['node_modules/(?!static-container)/']
};

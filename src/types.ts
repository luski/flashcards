export interface Group {
  id: string;
  name: string;
  count: number;
  disabled: boolean | undefined;
}

export interface SideContent {
  text: string;
  image?: string;
}

export interface FlashcardSides {
  frontSide: SideContent;
  backSide: SideContent;
}

export interface Flashcard extends FlashcardSides {
  id: string;
  groupId: string;
  stars: number;
  time: number;
}

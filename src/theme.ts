import {DarkTheme as PaperDarkTheme, DefaultTheme as PaperDefaultTheme, Colors} from 'react-native-paper';
import {DarkTheme as NativeDarkTheme, DefaultTheme as NativeLightTheme} from '@react-navigation/native';

const DarkTheme = {
  ...PaperDarkTheme,
  colors: {
    ...PaperDarkTheme.colors,
    primary: Colors.blue300,
    accent: Colors.blue300
  }
};

const LightTheme = {
  ...PaperDefaultTheme,
  colors: {
    ...PaperDefaultTheme.colors,
    primary: Colors.blue600,
    accent: Colors.blue600
  }
};

export const getTheme = (darkMode: boolean) => (darkMode ? DarkTheme : LightTheme);

export const getNativeTheme = (darkMode: boolean) =>
  darkMode
    ? {
        ...NativeDarkTheme,
        colors: {
          primary: DarkTheme.colors.primary,
          background: DarkTheme.colors.background,
          card: DarkTheme.colors.background,
          border: DarkTheme.colors.backdrop,
          text: DarkTheme.colors.text,
          notification: DarkTheme.colors.notification
        }
      }
    : {
        ...NativeLightTheme,
        colors: {
          primary: LightTheme.colors.primary,
          background: LightTheme.colors.background,
          card: LightTheme.colors.background,
          border: LightTheme.colors.backdrop,
          text: LightTheme.colors.text,
          notification: LightTheme.colors.notification
        }
      };

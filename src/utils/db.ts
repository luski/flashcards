import AsyncStorage from '@react-native-community/async-storage';
// @ts-ignore
import {nanoid} from 'nanoid/async/index.native';
import {Group, Flashcard, FlashcardSides} from '../types';
import flags from './flags.json';

const DBVERSION = '0.1';
const DBVERSION_KEY = 'dbVersion';

export async function initialize(): Promise<void> {
  const result = await AsyncStorage.getItem(DBVERSION_KEY);
  if (result !== DBVERSION) {
    await AsyncStorage.setItem(DBVERSION_KEY, DBVERSION);
    const group = await createGroup('European Flags');
    for (let {name, image} of flags) {
      await addFlashcard({frontSide: {text: '', image}, backSide: {text: name}}, group.id);
    }
  }
}

export async function createGroup(name: string): Promise<Group> {
  const groups = await getGroups();
  const group = {
    id: await nanoid(),
    name,
    count: 0,
    disabled: false
  };
  await AsyncStorage.setItem('groups', JSON.stringify([...groups, group]));
  return group;
}

export async function getGroups(): Promise<Group[]> {
  const value = await AsyncStorage.getItem('groups');
  if (value) return JSON.parse(value) as Group[];
  return [];
}

export async function renameGroup(groupId: string, name: string): Promise<void> {
  const groups = await getGroups();
  await AsyncStorage.setItem('groups', JSON.stringify(groups.map(g => (g.id === groupId ? {...g, name} : g))));
}

export async function deleteGroup(groupId: string): Promise<void> {
  const groups = await getGroups();
  await AsyncStorage.setItem('groups', JSON.stringify(groups.filter(group => group.id !== groupId)));
  await AsyncStorage.removeItem(`group-${groupId}`);
}

export async function toggleGroup(groupId: string): Promise<void> {
  const groups = await getGroups();
  await AsyncStorage.setItem(
    'groups',
    JSON.stringify(groups.map(group => (group.id === groupId ? {...group, disabled: !group.disabled} : group)))
  );
}

async function updateGroupCount(groupId: string, modifier: (count: number) => number): Promise<void> {
  const groups = await getGroups();
  await AsyncStorage.setItem(
    'groups',
    JSON.stringify(groups.map(group => (group.id === groupId ? {...group, count: modifier(group.count)} : group)))
  );
}

export async function getFlashcards(groupId: string): Promise<Flashcard[]> {
  const value = await AsyncStorage.getItem(`group-${groupId}`);
  if (value) return JSON.parse(value).map((flashcardData: any) => ({...flashcardData, groupId} as Flashcard));
  return [];
}

async function insertFlashcard(flashcard: Flashcard): Promise<Flashcard> {
  const {groupId, ...flashcardData} = flashcard;
  const groupFlashcards = await getFlashcards(groupId);
  const value = JSON.stringify([...groupFlashcards, flashcardData]);
  await AsyncStorage.setItem(`group-${groupId}`, value);
  await updateGroupCount(groupId, count => count + 1);
  return flashcard;
}

export async function addFlashcard(sides: FlashcardSides, groupId: string): Promise<Flashcard> {
  return await insertFlashcard({...sides, id: await nanoid(), stars: 0, time: 0, groupId});
}

export async function updateFlashcard(flashcardId: string, groupId: string, sides: FlashcardSides): Promise<void> {
  const groupFlashcards = await getFlashcards(groupId);
  const value = JSON.stringify(groupFlashcards.map(f => (f.id === flashcardId ? {...f, ...sides} : f)));
  await AsyncStorage.setItem(`group-${groupId}`, value);
}

export async function deleteFlashcard(flashcardId: string, groupId: string): Promise<void> {
  const groupFlashcards = await getFlashcards(groupId);
  const value = JSON.stringify(groupFlashcards.filter(({id}) => id !== flashcardId));
  await AsyncStorage.setItem(`group-${groupId}`, value);
  await updateGroupCount(groupId, count => count - 1);
}

export async function changeGroup(flashcardId: string, oldGroupId: string, newGroupId: string): Promise<void> {
  if (oldGroupId === newGroupId) {
    return;
  }
  const flashcards = await getFlashcards(oldGroupId);
  const flashcard = flashcards.find(({id}) => id === flashcardId);
  if (flashcard) {
    await deleteFlashcard(flashcardId, oldGroupId);
    await insertFlashcard({...flashcard, groupId: newGroupId});
  }
}

export async function updateFlashcardStars(flashcardId: string, groupId: string, modifier: (stars: number) => number) {
  const groupFlashcards = await getFlashcards(groupId);
  const value = JSON.stringify(
    groupFlashcards.map(f => (f.id === flashcardId ? {...f, stars: modifier(f.stars), time: Date.now()} : f))
  );
  await AsyncStorage.setItem(`group-${groupId}`, value);
}

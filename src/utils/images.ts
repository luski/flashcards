import {
  launchCameraAsync,
  launchImageLibraryAsync,
  requestCameraPermissionsAsync,
  requestCameraRollPermissionsAsync,
  ImagePickerOptions,
  ImagePickerResult
} from 'expo-image-picker';
import {PermissionResponse} from 'unimodules-permissions-interface';

async function getImage(
  requestPermissions: () => Promise<PermissionResponse>,
  perform: (options: ImagePickerOptions) => Promise<ImagePickerResult>
): Promise<string | undefined> {
  const {status} = await requestPermissions();
  if (status === 'granted') {
    const result = await perform({
      allowsEditing: true,
      allowsMultipleSelection: false,
      base64: true,
      quality: 0.15
    });

    if (!result.cancelled) {
      return result.base64;
    }
    throw new Error('cancelled');
  } else {
    throw new Error('denied');
  }
}

export async function pickImage() {
  return await getImage(requestCameraRollPermissionsAsync, launchImageLibraryAsync);
}

export async function takePhoto() {
  return await getImage(requestCameraPermissionsAsync, launchCameraAsync);
}

export function getImageUri(image: string) {
  if (image.startsWith('http://') || image.startsWith('https://') || image.startsWith('ftp://')) {
    return image;
  }
  return 'data:text/plain;base64,' + image;
}

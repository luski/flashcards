import {useCallback} from 'react';
import {StackNavigationOptions} from '@react-navigation/stack';

interface Navigation {
  setOptions: (options: Partial<StackNavigationOptions>) => void;
}

export default function useScreenOptionsChanger(navigation: Navigation) {
  return useCallback(
    (options: Partial<StackNavigationOptions>) => {
      navigation.setOptions(options);
    },
    [navigation]
  );
}

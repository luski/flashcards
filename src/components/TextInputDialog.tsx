import React, {useContext, useState} from 'react';
import {Dialog} from 'react-native-simple-dialogs';
import {StyleSheet, View} from 'react-native';
import {Button, TextInput} from 'react-native-paper';
import {SettingsContext} from '../contexts/SettingsProvider';
import {getTheme} from '../theme';
import {Trans} from 'react-i18next';

interface Props {
  visible: boolean;
  title: string;
  label: string;
  onTouchOutside: () => void;
  initialValue: string;
  onOk: (text: string) => void;
  onCancel: () => void;
}

export default function TextInputDialog({visible, title, onTouchOutside, initialValue, onOk, onCancel}: Props) {
  const [text, setText] = useState<string>(initialValue);
  const {darkMode} = useContext(SettingsContext);
  const theme = getTheme(darkMode);

  return (
    <Dialog
      visible={visible}
      title={title}
      onTouchOutside={onTouchOutside}
      titleStyle={{color: theme.colors.text}}
      dialogStyle={{backgroundColor: theme.colors.background}}
      // @ts-ignore
      children={
        <View style={styles.content}>
          <TextInput value={text} onChangeText={setText} autoFocus />
          <View style={styles.actions}>
            <View style={styles.actionWrapper}>
              <Button onPress={() => onOk(text)} color={theme.colors.primary}>
                <Trans i18nKey="ok">OK</Trans>
              </Button>
            </View>
            <View style={styles.actionWrapper}>
              <Button onPress={onCancel} color={theme.colors.primary}>
                <Trans i18nKey="cancel">Cancel</Trans>
              </Button>
            </View>
          </View>
        </View>
      }
    />
  );
}
const styles = StyleSheet.create({
  content: {
    paddingBottom: 20
  },
  actions: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    minHeight: 40,
    marginTop: 20
  },
  actionWrapper: {
    minHeight: 20
  },
  button: {
    width: '30%'
  }
});

import React, {useEffect, useRef, useState} from 'react';
import {Animated, PanResponder, PanResponderGestureState, StyleSheet} from 'react-native';

const V = 160;

interface Props {
  frontFace: React.ReactNode;
  backFace: React.ReactNode;
  hidden: boolean;
}

export default function Flashcard({frontFace, backFace, hidden}: Props) {
  const [pageReversed, setPageReversed] = useState(false);
  const rotationAnim = useRef(new Animated.Value(0)).current;
  const scaleXAnim = useRef(new Animated.Value(1)).current;
  const hidingAnim = useRef(new Animated.Value(1)).current;
  const dxRef = useRef(0);

  const panResponder = useRef(
    PanResponder.create({
      onMoveShouldSetPanResponder: () => true,
      onPanResponderMove: Animated.event([null, {dx: rotationAnim}], {
        // @ts-ignore according to TS types the listener shouldn't get the second param, but in fact it gets it.
        listener: (event: any, {dx}: PanResponderGestureState) => {
          const prevDx = dxRef.current;
          if ((prevDx > -V && dx <= -V) || (prevDx < V && dx >= V)) {
            scaleXAnim.setValue(-1);
            setPageReversed(reversed => !reversed);
          } else if ((prevDx > V && dx <= V) || (prevDx < -V && dx >= -V)) {
            scaleXAnim.setValue(1);
            setPageReversed(reversed => !reversed);
          }
          dxRef.current = dx;
        },
        useNativeDriver: false
      }),
      onPanResponderRelease: (e, {dx, vx}) => {
        if (dx < -V) {
          Animated.timing(rotationAnim, {toValue: -(2 * V), useNativeDriver: true}).start(() => {
            rotationAnim.setValue(0);
            scaleXAnim.setValue(1);
            dxRef.current = 0;
          });
        } else if (dx < V) {
          if ((dx > 0 && vx > 0.1) || (dx < 0 && vx < -0.1)) {
            const sign = Math.sign(vx);
            Animated.sequence([
              Animated.timing(rotationAnim, {toValue: sign * V, useNativeDriver: true, duration: 40}),
              Animated.spring(rotationAnim, {toValue: sign * 2 * V, useNativeDriver: true})
            ]).start(() => {
              rotationAnim.setValue(0);
              scaleXAnim.setValue(1);
              dxRef.current = 0;
            });
            setTimeout(() => {
              setPageReversed(reversed => !reversed);
              scaleXAnim.setValue(-1);
            }, 20);
          } else {
            Animated.spring(rotationAnim, {toValue: 0, useNativeDriver: true}).start(() => {
              dxRef.current = 0;
            });
          }
        } else {
          Animated.timing(rotationAnim, {toValue: 2 * V, useNativeDriver: true}).start(() => {
            rotationAnim.setValue(0);
            scaleXAnim.setValue(1);
            dxRef.current = 0;
          });
        }
      }
    })
  ).current;

  useEffect(() => {
    if (hidden) {
      Animated.timing(hidingAnim, {toValue: 0, duration: 200, useNativeDriver: true}).start();
    } else {
      setPageReversed(false);
      Animated.timing(hidingAnim, {toValue: 1, duration: 200, useNativeDriver: true}).start();
    }
  }, [hidden]);

  const cardStyle = StyleSheet.flatten([
    styles.card,
    {
      opacity: hidingAnim,
      transform: [
        {
          rotateY: rotationAnim.interpolate({inputRange: [0, 100], outputRange: [0, 1]}),
          scaleX: scaleXAnim
        }
      ]
    }
  ]);

  return (
    <Animated.View style={cardStyle} {...panResponder.panHandlers}>
      {pageReversed ? backFace : frontFace}
    </Animated.View>
  );
}

const styles = StyleSheet.create({
  card: {
    width: '100%',
    height: '100%',
    borderColor: 'black',
    borderWidth: 1
  }
});

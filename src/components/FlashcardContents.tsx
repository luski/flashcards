import React, {useContext} from 'react';
import {StyleSheet, View, Image} from 'react-native';
import {Divider, Subheading, Title} from 'react-native-paper';
import Markdown from 'react-native-markdown-display';

import {SettingsContext} from '../contexts/SettingsProvider';
import {getTheme} from '../theme';
import {SideContent} from '../types';
import {getImageUri} from '../utils/images';

interface Props {
  title: string;
  stars: number;
  content: SideContent;
}

const starsToText = (stars: number) => {
  return new Array(5)
    .fill(null)
    .map((_, num) => (num < stars ? '★' : '☆'))
    .join('');
};

export default function FlashcardContents({title, stars, content: {image, text}}: Props) {
  const {darkMode} = useContext(SettingsContext);
  const theme = getTheme(darkMode);

  return (
    <View style={{...styles.content, backgroundColor: theme.colors.background}}>
      <View style={styles.header}>
        <Title style={styles.headerText}>{title}</Title>
        <Subheading style={styles.headerText}>{starsToText(stars)}</Subheading>
      </View>
      <Divider />
      <View style={styles.middle}>
        {image && <Image style={styles.image} source={{uri: getImageUri(image)}} resizeMode="contain" />}
        <View style={styles.textContainer}>
          <Markdown
            style={{
              body: {
                fontSize: 25,
                color: theme.colors.text
              }
            }}>
            {text}
          </Markdown>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
    flexDirection: 'column',
    padding: 20
  },
  header: {
    marginBottom: 20
  },
  headerText: {
    textAlign: 'center'
  },
  middle: {
    flexGrow: 1,
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  image: {
    flexGrow: 1,
    width: '100%'
  },
  textContainer: {},
  footer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexGrow: 0,
    paddingVertical: 20,
    marginTop: 20
  }
});

import {ConfirmDialog as RNConfirmDialog, ConfirmDialogProps} from 'react-native-simple-dialogs';
import React, {useContext} from 'react';
import {SettingsContext} from '../contexts/SettingsProvider';
import {getTheme} from '../theme';

export default function ConfirmDialog(props: ConfirmDialogProps) {
  const {darkMode} = useContext(SettingsContext);
  const theme = getTheme(darkMode);

  return (
    <RNConfirmDialog
      titleStyle={{color: theme.colors.text}}
      dialogStyle={{backgroundColor: theme.colors.background}}
      messageStyle={{color: theme.colors.text}}
      {...props}
    />
  );
}

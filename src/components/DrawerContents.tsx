import React, {useContext, useMemo} from 'react';
import {StyleSheet, View} from 'react-native';
import {Trans, useTranslation} from 'react-i18next';
import {DrawerContentScrollView} from '@react-navigation/drawer';
import {Avatar, Paragraph, Title, Drawer, Text, Caption, Switch} from 'react-native-paper';
import {DrawerNavigationHelpers} from '@react-navigation/drawer/lib/typescript/src/types';

import {DataContext} from '../contexts/DataProvider';
import {SettingsContext} from '../contexts/SettingsProvider';

const Header = () => (
  <View style={styles.header}>
    <Avatar.Image source={require('../../assets/icon.png')} style={{backgroundColor: 'transparent'}} />
    <Title style={styles.title}>
      <Trans i18nKey="home.title">Flashcards</Trans>
    </Title>
  </View>
);

const Stats = () => {
  const {flashcardBuckets} = useContext(DataContext);

  const all = useMemo(() => flashcardBuckets.reduce((acc, bucket) => acc + bucket.length, 0), [flashcardBuckets]);

  return (
    <Drawer.Section>
      <View style={styles.stats}>
        <View style={styles.row}>
          <View style={styles.rowItem}>
            <Paragraph>{all}</Paragraph>
            <Caption>
              <Trans i18nKey="drawer.stats.flashcards">flashcards</Trans>
            </Caption>
          </View>
          <View style={styles.rowItem}>
            <Paragraph>{flashcardBuckets[5].length}</Paragraph>
            <Caption>
              <Trans i18nKey="drawer.stats.learned">learned</Trans>
            </Caption>
          </View>
        </View>
        <View style={styles.row}>
          <View style={styles.rowItem}>
            <Paragraph>{all - flashcardBuckets[5].length}</Paragraph>
            <Caption>
              <Trans i18nKey="drawer.stats.learning">learning</Trans>
            </Caption>
          </View>
          <View style={styles.rowItem}>
            <Paragraph>{flashcardBuckets[0].length}</Paragraph>
            <Caption>
              <Trans i18nKey="drawer.stats.unknown">unknown</Trans>
            </Caption>
          </View>
        </View>
      </View>
    </Drawer.Section>
  );
};

interface Props {
  navigation: DrawerNavigationHelpers;
}

const Actions = ({navigation}: Props) => {
  const {darkMode, setDarkMode} = useContext(SettingsContext);
  const {t} = useTranslation();

  return (
    <>
      <Drawer.Item
        label={t('groups.title', {defaultValue: 'Groups'})}
        icon="shape"
        onPress={() => navigation.navigate('Groups')}
      />
      <Drawer.Item
        label={t('drawer.addFlashcard', {defaultValue: 'Add a flashcard'})}
        icon="plus-box"
        onPress={() => navigation.navigate('Edit', {initialValues: null})}
      />
      <Drawer.Section style={styles.settings}>
        <Caption>
          <Trans i18nKey="drawer.settings">Settings</Trans>
        </Caption>
      </Drawer.Section>
      <View style={styles.switchRow}>
        <Text>
          <Trans i18nKey="drawer.darkMode">Dark mode</Trans>
        </Text>
        <Switch value={darkMode} onValueChange={setDarkMode} />
      </View>
    </>
  );
};

interface Props {
  navigation: DrawerNavigationHelpers;
}

export default function DrawerContents({navigation}: Props) {
  return (
    <DrawerContentScrollView>
      <View style={styles.drawerContent}>
        <Header />
        <Stats />
        <Actions navigation={navigation} />
      </View>
    </DrawerContentScrollView>
  );
}

const styles = StyleSheet.create({
  drawerContent: {
    flex: 1,
    paddingLeft: 20
  },
  header: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  title: {
    marginLeft: 20
  },
  stats: {
    marginTop: 70,
    marginBottom: 50,
    marginLeft: 10,
    marginRight: 40
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginVertical: 10
  },
  rowItem: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center'
  },
  settings: {
    marginTop: 40
  },
  switchRow: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20
  }
});

import {createContext} from 'react';
import {SideContent} from '../../types';

export interface FlashcardValues {
  flashcardId: string;
  frontSide: SideContent;
  backSide: SideContent;
  groupId: string;
}

export type DeleteButtonHandler = () => void;

interface ContextType {
  setFlashcardValues: (flashcardValues: FlashcardValues | null) => void;
  setDeleteButtonHandler: (handler: DeleteButtonHandler | null) => void;
}

export const HomeScreenContext = createContext<ContextType>({
  setFlashcardValues: () => {},
  setDeleteButtonHandler: () => {}
});

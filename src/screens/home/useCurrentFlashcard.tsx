import {useContext, useEffect, useMemo, useState} from 'react';
import {DataContext} from '../../contexts/DataProvider';
import {Flashcard} from '../../types';

const DELAY = 60 * 1000;
const REFRESH_INTERVAL = 10 * 1000;

export default function useCurrentFlashcard() {
  const {flashcardBuckets: allFlashcardBuckets, groups} = useContext(DataContext);
  const [currentFlashcard, setCurrentFlashcard] = useState<Flashcard | null>(null);
  const [updateTrigger, setUpdateTrigger] = useState(0);

  useEffect(() => {
    const intervalId = setInterval(() => {
      setUpdateTrigger(t => t + 1);
    }, REFRESH_INTERVAL);

    return () => clearInterval(intervalId);
  }, []);
  const disabledGroupIds = useMemo(() => groups.filter(({disabled}) => !!disabled).map(({id}) => id), [groups]);

  const enabledFlashcardBuckets = useMemo(
    () => allFlashcardBuckets.map(bucket => bucket.filter(({groupId}) => !disabledGroupIds.includes(groupId))),
    [allFlashcardBuckets, disabledGroupIds]
  );

  const flashcardBuckets = useMemo(() => {
    const now = Date.now();
    return enabledFlashcardBuckets.map(bucket =>
      bucket.filter(({time, groupId}) => !disabledGroupIds.includes(groupId) && now - time > DELAY)
    );
  }, [enabledFlashcardBuckets, disabledGroupIds, updateTrigger]);

  useEffect(() => {
    if (currentFlashcard && flashcardBuckets.some(bucket => bucket.some(f => f.id === currentFlashcard.id))) {
      return;
    }
    for (let i = 0; i < 6; i++) {
      const bucket = flashcardBuckets[i];
      const flashcard = bucket[Math.floor(Math.random() * bucket.length)];
      if (flashcard) {
        setCurrentFlashcard(flashcard);
        return;
      }
    }
    setCurrentFlashcard(null);
  }, [flashcardBuckets, currentFlashcard]);

  return currentFlashcard;
}

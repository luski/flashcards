import React, {useCallback, useContext, useEffect, useMemo, useState} from 'react';
import {Button, Colors, IconButton} from 'react-native-paper';
import {ImageBackground, StyleSheet, Text, View} from 'react-native';
import {Trans, useTranslation} from 'react-i18next';
import {DrawerScreenProps} from '@react-navigation/drawer';
import Toast from 'react-native-root-toast';

import FlashcardView from '../../components/Flashcard';
import FlashcardContents from '../../components/FlashcardContents';
import {HomeScreenContext} from './HomeScreenContext';
import {DataContext, StarsUpdate} from '../../contexts/DataProvider';
import {ParamsList} from '../../Router';
import {SettingsContext} from '../../contexts/SettingsProvider';
import {getTheme} from '../../theme';
import ConfirmDialog from '../../components/ConfirmDialog';
import useCurrentFlashcard from './useCurrentFlashcard';

export default function HomeScreen({navigation}: DrawerScreenProps<ParamsList, 'NoArgs'>) {
  const {t} = useTranslation();
  const {setDeleteButtonHandler, setFlashcardValues} = useContext(HomeScreenContext);
  const {flashcardBuckets, deleteFlashcard, updateFlashcardStars, groups} = useContext(DataContext);
  const [confirmDeleteVisible, setConfirmDeleteVisible] = useState(false);
  const [hidden, setHidden] = useState(false);
  const {darkMode} = useContext(SettingsContext);
  const theme = getTheme(darkMode);
  const currentFlashcard = useCurrentFlashcard();
  const disabledGroupIds = useMemo(() => groups.filter(({disabled}) => !!disabled).map(({id}) => id), [groups]);
  const enabledFlashcardBuckets = useMemo(
    () => flashcardBuckets.map(bucket => bucket.filter(({groupId}) => !disabledGroupIds.includes(groupId))),
    [flashcardBuckets, disabledGroupIds]
  );

  useEffect(() => {
    setFlashcardValues(
      currentFlashcard
        ? {
            frontSide: currentFlashcard.frontSide,
            backSide: currentFlashcard.backSide,
            flashcardId: currentFlashcard.id,
            groupId: currentFlashcard.groupId
          }
        : null
    );
  }, [setFlashcardValues, currentFlashcard]);

  useEffect(() => {
    setDeleteButtonHandler(() => setConfirmDeleteVisible(true));
  }, [setDeleteButtonHandler]);

  const groupName = groups.find(({id}) => id === currentFlashcard?.groupId)?.name ?? '';

  const performFlashcardChange = useCallback(async (operation: () => Promise<void>) => {
    setHidden(true);
    setTimeout(async () => {
      await operation();
      setHidden(false);
    }, 100);
  }, []);

  if (!currentFlashcard && enabledFlashcardBuckets.every(bucket => bucket.length === 0)) {
    return (
      <>
        <View style={{height: '100%', flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{color: Colors.grey600, fontSize: 20, textAlign: 'center', margin: 50}}>
            <Trans i18nKey="home.noFlashcards1">You haven't created any flashcards yet</Trans>
          </Text>
          <Text
            style={{color: Colors.grey600, fontSize: 12, textAlign: 'center', marginHorizontal: 50, marginBottom: 50}}>
            <Trans i18nKey="home.noFlashcards2">...or all groups are disabled</Trans>
          </Text>

          <Button icon="plus" mode="contained" onPress={() => navigation.navigate('Edit', {initialValues: undefined})}>
            <Trans i18nKey="home.addFlashcard">Add Flashcard</Trans>
          </Button>
        </View>
      </>
    );
  }

  if (!currentFlashcard) {
    return (
      <View style={{height: '100%', flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text style={{color: Colors.grey600, fontSize: 20, textAlign: 'center', margin: 50}}>
          <Trans i18nKey="home.allSolved1">You've already solved all your flashcards. Congrats!</Trans>
        </Text>
        <Text style={{color: Colors.grey600, fontSize: 12, textAlign: 'center', margin: 50}}>
          <Trans i18nKey="home.allSolved2">Wait a moment or check the app later...</Trans>
        </Text>
      </View>
    );
  }

  const bgSource = darkMode ? require('../../../assets/bg-dark.jpg') : require('../../../assets/bg-light.png');

  return (
    <>
      <ImageBackground source={bgSource} style={styles.backgroundStyles}>
        <View style={styles.flashcard}>
          <FlashcardView
            frontFace={
              <FlashcardContents
                title={groupName}
                stars={currentFlashcard.stars}
                content={currentFlashcard.frontSide}
              />
            }
            backFace={
              <FlashcardContents title={groupName} stars={currentFlashcard.stars} content={currentFlashcard.backSide} />
            }
            hidden={hidden}
          />
        </View>
        <View style={styles.footer}>
          <IconButton
            icon="thumb-down"
            color={Colors.red800}
            size={40}
            onPress={() =>
              performFlashcardChange(() =>
                updateFlashcardStars(currentFlashcard.id, currentFlashcard?.groupId, StarsUpdate.DOWN)
              )
            }
          />
          <IconButton
            icon="skip-forward"
            color={Colors.amber300}
            size={20}
            onPress={() =>
              performFlashcardChange(() =>
                updateFlashcardStars(currentFlashcard.id, currentFlashcard?.groupId, StarsUpdate.SAME)
              )
            }
          />
          <IconButton
            icon="thumb-up"
            color={Colors.green300}
            size={40}
            onPress={() =>
              performFlashcardChange(() =>
                updateFlashcardStars(currentFlashcard.id, currentFlashcard?.groupId, StarsUpdate.UP)
              )
            }
          />
        </View>
      </ImageBackground>
      <ConfirmDialog
        title={t('home.deleteModal.title', {defaultValue: 'Please confirm'})}
        message={t('home.deleteModal.message', {defaultValue: 'Are you sure you want to delete this flashcard?'})}
        visible={confirmDeleteVisible}
        onTouchOutside={() => setConfirmDeleteVisible(false)}
        positiveButton={{
          title: t('yes', {defaultValue: 'Yes'}),
          titleStyle: {color: theme.colors.accent},
          onPress: async () => {
            await deleteFlashcard(currentFlashcard.id, currentFlashcard?.groupId);
            setConfirmDeleteVisible(false);
            Toast.show(t('home.flashcardDeleted', {defaultValue: 'Flashcard deleted'}));
          }
        }}
        negativeButton={{
          title: t('no', {defaultValue: 'No'}),
          titleStyle: {color: theme.colors.accent},
          onPress: () => setConfirmDeleteVisible(false)
        }}
      />
    </>
  );
}
const styles = StyleSheet.create({
  backgroundStyles: {
    height: '100%'
  },
  flashcard: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20
  },
  footer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexGrow: 0,
    paddingVertical: 30,
    width: '100%',
    backfaceVisibility: 'hidden'
  }
});

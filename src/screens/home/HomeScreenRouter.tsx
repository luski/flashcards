import React, {useCallback, useLayoutEffect, useRef, useState} from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {StackScreenProps} from '@react-navigation/stack';
import {DrawerActions} from '@react-navigation/native';
import {IconButton} from 'react-native-paper';

import DrawerContents from '../../components/DrawerContents';
import HomeScreen from './HomeScreen';
import {ParamsList} from '../../Router';
import {DeleteButtonHandler, FlashcardValues, HomeScreenContext} from './HomeScreenContext';
import {MenuHeaderButton} from './MenuHeaderButton';

const Drawer = createDrawerNavigator();

export default function HomeScreenRouter({navigation}: StackScreenProps<ParamsList, 'NoArgs'>) {
  const deleteButtonRef = useRef<DeleteButtonHandler | null>(null);
  const flashcardValuesRef = useRef<FlashcardValues | null>(null);
  const [updateTrigger, setUpdateTrigger] = useState(0);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => <IconButton icon="menu" onPress={() => navigation.dispatch(DrawerActions.toggleDrawer())} />,
      headerRight: () =>
        flashcardValuesRef.current ? (
          <MenuHeaderButton
            onDelete={deleteButtonRef.current || (() => {})}
            onEdit={() => navigation.navigate('Edit', {initialValues: flashcardValuesRef.current || undefined})}
          />
        ) : undefined
    });
  }, [navigation, updateTrigger]);

  const handleFlashcardValuesChange = useCallback(values => {
    flashcardValuesRef.current = values;
    setUpdateTrigger(t => t + 1);
  }, []);

  const handleDeleteButtonHandlerChange = useCallback(handler => {
    deleteButtonRef.current = handler;
    setUpdateTrigger(t => t + 1);
  }, []);

  return (
    <HomeScreenContext.Provider
      value={{
        setFlashcardValues: handleFlashcardValuesChange,
        setDeleteButtonHandler: handleDeleteButtonHandlerChange
      }}>
      <Drawer.Navigator
        initialRouteName="Home"
        drawerContent={({navigation}) => <DrawerContents navigation={navigation} />}>
        <Drawer.Screen name="Home" component={HomeScreen} />
      </Drawer.Navigator>
    </HomeScreenContext.Provider>
  );
}

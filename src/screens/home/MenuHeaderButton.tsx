import React, {useContext, useState} from 'react';
import {Appbar, Menu} from 'react-native-paper';
import {useTranslation} from 'react-i18next';

import {SettingsContext} from '../../contexts/SettingsProvider';
import {getTheme} from '../../theme';

interface Props {
  onDelete: () => void;
  onEdit: () => void;
}

export const MenuHeaderButton = ({onDelete, onEdit}: Props) => {
  const [visible, setVisible] = useState(false);
  const {darkMode} = useContext(SettingsContext);
  const theme = getTheme(darkMode);
  const {t} = useTranslation();

  return (
    <Menu
      visible={visible}
      onDismiss={() => setVisible(false)}
      anchor={<Appbar.Action icon="dots-vertical" color={theme.colors.text} onPress={() => setVisible(true)} />}>
      <Menu.Item
        onPress={() => {
          onEdit();
          setVisible(false);
        }}
        icon="pencil"
        title={t('edit', {defaultValue: 'Edit'})}
      />
      <Menu.Item
        onPress={() => {
          onDelete();
          setVisible(false);
        }}
        icon="delete"
        title={t('delete', {defaultValue: 'Delete'})}
      />
    </Menu>
  );
};

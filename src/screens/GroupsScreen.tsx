import React, {useContext, useLayoutEffect, useState} from 'react';
import {ActivityIndicator, Colors, IconButton, List, Menu, Switch, Text} from 'react-native-paper';
import {FlatList, View} from 'react-native';
import Toast from 'react-native-root-toast';
import {StackNavigationOptions, StackScreenProps} from '@react-navigation/stack';

import {ParamsList} from '../Router';
import {Group} from '../types';
import TextInputDialog from '../components/TextInputDialog';
import useScreenOptionsChanger from '../hooks/useScreenOptionsChanger';
import {DataContext} from '../contexts/DataProvider';
import {SettingsContext} from '../contexts/SettingsProvider';
import {getTheme} from '../theme';
import ConfirmDialog from '../components/ConfirmDialog';
import {Trans, useTranslation} from 'react-i18next';

interface MenuButtonProps {
  onDelete: () => void;
  onRename: () => void;
}

const MenuButton = ({onDelete, onRename}: MenuButtonProps) => {
  const [visible, setVisible] = useState(false);
  const {t} = useTranslation();

  return (
    <Menu
      visible={visible}
      onDismiss={() => setVisible(false)}
      anchor={<IconButton icon="dots-vertical" onPress={() => setVisible(true)} />}>
      <Menu.Item
        icon="pencil"
        onPress={() => {
          onRename();
          setVisible(false);
        }}
        title={t('groups.rename', {defaultValue: 'Rename'})}
      />
      <Menu.Item
        icon="delete"
        onPress={() => {
          onDelete();
          setVisible(false);
        }}
        title={t('groups.delete', {defaultValue: 'Delete'})}
      />
    </Menu>
  );
};

interface Props {
  groups: Group[];
  loading: boolean;
  onScreenOptionsChange: (options: Partial<StackNavigationOptions>) => void;
  onCreate: (name: string) => void;
  onRename: (groupId: string, name: string) => void;
  onDelete: (groupId: string) => void;
  onToggle: (groupId: string) => void;
}

function GroupsScreenView({groups, loading, onScreenOptionsChange, onDelete, onCreate, onRename, onToggle}: Props) {
  const {t} = useTranslation();
  const [confirmDeleteVisible, setConfirmDeleteVisible] = useState(false);
  const [createGroupDialogVisible, setCreateGroupDialogVisible] = useState(false);
  const [renameGroupDialogVisible, setRenameGroupDialogVisible] = useState(false);
  const [groupId, setGroupId] = useState<string | null>(null);
  const {darkMode} = useContext(SettingsContext);
  const theme = getTheme(darkMode);

  useLayoutEffect(() => {
    onScreenOptionsChange({
      headerRight: () => <IconButton icon="plus" onPress={() => setCreateGroupDialogVisible(true)} disabled={loading} />
    });
  }, [onScreenOptionsChange]);

  return loading ? (
    <ActivityIndicator testID="spinner" style={{margin: 20}} />
  ) : (
    <>
      {groups.length ? (
        <FlatList
          data={groups}
          renderItem={({item: {name, count, id, disabled}}) => (
            <List.Item
              title={name}
              description={t('groups.count', {count})}
              left={() => <Switch value={!disabled} onValueChange={() => onToggle(id)} />}
              right={() => (
                <MenuButton
                  onDelete={() => {
                    setGroupId(id);
                    setConfirmDeleteVisible(true);
                  }}
                  onRename={() => {
                    setGroupId(id);
                    setRenameGroupDialogVisible(true);
                  }}
                />
              )}
            />
          )}
        />
      ) : (
        <View style={{height: '100%', flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{color: Colors.grey600, fontSize: 20, textAlign: 'center', margin: 50}}>
            <Trans i18nKey="groups.empty">You haven't created any groups yet</Trans>
          </Text>
        </View>
      )}

      {groupId && (
        <ConfirmDialog
          title={t('groups.deleteModal.title', {defaultValue: 'Please confirm'})}
          message={t('groups.deleteModal.message', {
            defaultValue: 'Are you sure you want to delete this group along with all belonging flashcards?'
          })}
          visible={confirmDeleteVisible}
          onTouchOutside={() => setConfirmDeleteVisible(false)}
          positiveButton={{
            title: t('yes', {defaultValue: 'Yes'}),
            titleStyle: {color: theme.colors.accent},
            onPress: () => {
              onDelete(groupId);
              setGroupId(null);
              setConfirmDeleteVisible(false);
              Toast.show(t('groups.groupDeleted', {defaultValue: 'Group deleted'}));
            }
          }}
          negativeButton={{
            title: t('no', {defaultValue: 'No'}),
            titleStyle: {color: theme.colors.accent},
            onPress: () => setConfirmDeleteVisible(false)
          }}
        />
      )}

      <TextInputDialog
        visible={createGroupDialogVisible}
        title={t('groups.createModal.title', {defaultValue: 'Create Group'})}
        label={t('groups.createModal.label', {defaultValue: 'Name'})}
        onTouchOutside={() => setCreateGroupDialogVisible(false)}
        initialValue=""
        onOk={(groupName: string) => {
          onCreate(groupName);
          setCreateGroupDialogVisible(false);
          Toast.show(t('groups.groupCreated', {defaultValue: 'Group created'}));
        }}
        onCancel={() => setCreateGroupDialogVisible(false)}
      />

      {groupId && renameGroupDialogVisible && (
        <TextInputDialog
          visible={renameGroupDialogVisible}
          title={t('groups.renameModal.title', {defaultValue: 'Rename Group'})}
          label={t('groups.renameModal.label', {defaultValue: 'Name'})}
          onTouchOutside={() => setRenameGroupDialogVisible(false)}
          initialValue={groups.find(({id}) => id === groupId)?.name || ''}
          onOk={(groupName: string) => {
            onRename(groupId, groupName);
            setGroupId(null);
            setCreateGroupDialogVisible(false);
            Toast.show(t('groups.groupRenamed', {defaultValue: 'Group renamed'}));
          }}
          onCancel={() => setRenameGroupDialogVisible(false)}
        />
      )}
    </>
  );
}

interface ContainerProps {
  onScreenOptionsChange: (options: Partial<StackNavigationOptions>) => void;
}

function GroupsScreenContainer({onScreenOptionsChange}: ContainerProps) {
  const [loading, setLoading] = useState(false);
  const {groups, createGroup, renameGroup, deleteGroup, toggleGroup} = useContext(DataContext);

  const handleCreate = async (groupName: string) => {
    setLoading(true);
    await createGroup(groupName);
    setLoading(false);
  };

  const handleRename = async (groupId: string, groupName: string) => {
    setLoading(true);
    await renameGroup(groupId, groupName);
    setLoading(false);
  };

  const handleDelete = async (groupId: string) => {
    setLoading(true);
    await deleteGroup(groupId);
    setLoading(false);
  };

  const handleToggle = async (groupId: string) => {
    setLoading(true);
    await toggleGroup(groupId);
    setLoading(false);
  };

  return (
    <GroupsScreenView
      groups={groups}
      loading={loading}
      onScreenOptionsChange={onScreenOptionsChange}
      onCreate={handleCreate}
      onRename={handleRename}
      onDelete={handleDelete}
      onToggle={handleToggle}
    />
  );
}

export default function GroupsScreen({navigation}: StackScreenProps<ParamsList, 'NoArgs'>) {
  return <GroupsScreenContainer onScreenOptionsChange={useScreenOptionsChanger(navigation)} />;
}

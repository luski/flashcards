import React, {useCallback, useContext, useEffect, useLayoutEffect, useState} from 'react';
import {ScrollView, StyleSheet, View} from 'react-native';
import {ActivityIndicator, Card, IconButton} from 'react-native-paper';
import {Picker} from '@react-native-community/picker';
import {StackNavigationOptions, StackScreenProps} from '@react-navigation/stack';
import Toast from 'react-native-root-toast';
import {useTranslation} from 'react-i18next';

import {ParamsList} from '../../Router';
import {Group, SideContent} from '../../types';
import {FlashcardValues} from '../home/HomeScreenContext';
import useScreenOptionsChanger from '../../hooks/useScreenOptionsChanger';
import TextInputDialog from '../../components/TextInputDialog';
import {DataContext} from '../../contexts/DataProvider';
import {SettingsContext} from '../../contexts/SettingsProvider';
import {getTheme} from '../../theme';
import SideCard from './SideCard';

interface Props {
  flashcardId: string | null;
  initialFrontSide: SideContent;
  initialBackSide: SideContent;
  initialGroupId: string | null;
  groups: Group[];
  loading?: boolean;
  onScreenOptionsChange: (options: Partial<StackNavigationOptions>) => void;
  onSave: (values: FlashcardValues) => void;
  onGroupCreate: (groupName: string) => Promise<Group>;
}

export const EditFlashcardScreenContents = ({
  groups,
  flashcardId,
  initialFrontSide,
  initialBackSide,
  initialGroupId,
  loading,
  onSave,
  onGroupCreate,
  onScreenOptionsChange
}: Props) => {
  const {t} = useTranslation();
  const [groupId, setGroupId] = useState('');
  const [frontSide, setFrontSide] = useState(initialFrontSide);
  const [backSide, setBackSide] = useState(initialBackSide);
  const [createGroupModalVisible, setCreateGroupModalVisible] = useState(false);
  const {darkMode} = useContext(SettingsContext);
  const theme = getTheme(darkMode);

  useEffect(() => {
    setGroupId(initialGroupId || '');
    setFrontSide(initialFrontSide);
    setBackSide(initialBackSide);
  }, [initialFrontSide, initialBackSide, initialGroupId]);

  const handleSave = useCallback(() => {
    onSave({
      groupId,
      frontSide,
      backSide,
      flashcardId: flashcardId ?? ''
    });
  }, [onSave, groupId, frontSide, backSide, flashcardId]);

  const changed = initialGroupId !== groupId || initialFrontSide !== frontSide || initialBackSide !== backSide;
  const valid = groupId && (frontSide.text.trim() || frontSide.image) && (backSide.text.trim() || backSide.image);

  useLayoutEffect(() => {
    onScreenOptionsChange({
      title: t(flashcardId ? 'edit.title.edit' : 'edit.title.create'),
      headerRight: () => <IconButton icon="content-save" onPress={handleSave} disabled={!valid || !changed} />
    });
  }, [onScreenOptionsChange, flashcardId, handleSave, changed]);

  return (
    <ScrollView>
      <View>
        {loading ? (
          <ActivityIndicator testID="spinner" />
        ) : (
          <>
            <Card style={{margin: 10}}>
              <Card.Title
                title="Grupa"
                right={() => <IconButton icon="plus" onPress={() => setCreateGroupModalVisible(true)} />}
              />
              <Card.Content>
                <View style={styles.row}>
                  <Picker
                    selectedValue={groupId}
                    onValueChange={v => setGroupId(v.toString())}
                    testID="group-id"
                    style={{color: theme.colors.text}}>
                    {groups.map(({id, name}) => (
                      <Picker.Item key={id} label={name} value={id} />
                    ))}
                  </Picker>
                </View>
              </Card.Content>
            </Card>
            <SideCard
              label={t('edit.frontSide', {defaultValue: 'Front Side'})}
              side={frontSide}
              onChange={setFrontSide}
            />
            <SideCard label={t('edit.backSide', {defaultValue: 'Back Side'})} side={backSide} onChange={setBackSide} />
          </>
        )}
      </View>
      <TextInputDialog
        visible={createGroupModalVisible}
        title={t('edit.createGroup', {defaultValue: 'Create Group'})}
        label={t('edit.groupName', {defaultValue: 'Name'})}
        onTouchOutside={() => setCreateGroupModalVisible(false)}
        initialValue=""
        onOk={(groupName: string) => {
          onGroupCreate(groupName).then(group => setGroupId(group.id));
          setCreateGroupModalVisible(false);
          Toast.show(t('edit.groupCreated', {defaultValue: 'Group created'}));
        }}
        onCancel={() => setCreateGroupModalVisible(false)}
      />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  row: {width: '100%', height: 70}
});

interface ContainerProps {
  initialValues?: FlashcardValues;
  onScreenOptionsChange: (options: Partial<StackNavigationOptions>) => void;
}

function EditFlashcardContainer({initialValues, onScreenOptionsChange}: ContainerProps) {
  const {groups, addFlashcard, changeFlashcardGroup, updateFlashcard, createGroup} = useContext(DataContext);
  const [loading, setLoading] = useState(false);
  const [flashcardId, setFlashcardId] = useState<string | null>(null);
  const [initialGroupId, setInitialGroupId] = useState<string | null>(null);
  const [initialFrontSide, setInitialFrontSide] = useState<SideContent>({text: ''});
  const [initialBackSide, setInitialBackSide] = useState<SideContent>({text: ''});

  useEffect(() => {
    if (initialValues) {
      setFlashcardId(initialValues.flashcardId);
      setInitialGroupId(initialValues.groupId);
      setInitialFrontSide(initialValues.frontSide);
      setInitialBackSide(initialValues.backSide);
    } else {
      setFlashcardId(null);
      setInitialGroupId(groups.length ? groups[0].id : null);
      setInitialFrontSide({text: ''});
      setInitialBackSide({text: ''});
    }
  }, [initialValues]);

  const handleSave = async ({frontSide, backSide, groupId}: FlashcardValues) => {
    setLoading(true);
    if (!flashcardId) {
      const flashcard = await addFlashcard({frontSide, backSide}, groupId);
      setFlashcardId(flashcard.id);
    } else {
      if (!initialGroupId) {
        throw new Error('EditFlashcardScreen: Initial data not provided');
      }
      if (initialGroupId !== groupId) {
        await changeFlashcardGroup(flashcardId, initialGroupId, groupId);
      }
      await updateFlashcard(flashcardId, initialGroupId, {frontSide, backSide});
    }
    setLoading(false);
  };

  const handleGroupCreate = async (groupName: string) => {
    setLoading(true);
    const group = await createGroup(groupName);
    setLoading(false);
    return group;
  };

  return (
    <EditFlashcardScreenContents
      groups={groups}
      flashcardId={flashcardId}
      initialFrontSide={initialFrontSide}
      initialBackSide={initialBackSide}
      initialGroupId={initialGroupId}
      loading={loading}
      onSave={handleSave}
      onGroupCreate={handleGroupCreate}
      onScreenOptionsChange={onScreenOptionsChange}
    />
  );
}

export default function EditFlashcardScreen({
  navigation,
  route: {
    params: {initialValues}
  }
}: StackScreenProps<ParamsList, 'Edit'>) {
  return (
    <EditFlashcardContainer initialValues={initialValues} onScreenOptionsChange={useScreenOptionsChanger(navigation)} />
  );
}

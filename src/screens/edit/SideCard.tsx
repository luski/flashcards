import React, {useState} from 'react';
import {Button, Card, Colors, TextInput} from 'react-native-paper';
import {Trans, useTranslation} from 'react-i18next';

import {SideContent} from '../../types';
import {getImageUri, pickImage, takePhoto} from '../../utils/images';

interface Props {
  side: SideContent;
  label: string;
  onChange: (side: SideContent) => void;
}

export default function SideCard({side, label, onChange}: Props) {
  const {t} = useTranslation();
  const [loading, setLoading] = useState(false);

  const getImageFromDevice = async (getter: () => Promise<string | undefined>) => {
    setLoading(true);
    try {
      const base64 = await getter();
      onChange({...side, image: base64});
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  const handlePictureSelect = () => getImageFromDevice(pickImage);
  const handleTakePhoto = () => getImageFromDevice(takePhoto);

  const handlePictureDelete = () => {
    const {image, ...newSide} = side;
    onChange(newSide);
  };

  return (
    <Card style={{marginBottom: 10, marginHorizontal: 10}}>
      <Card.Title title={label} />
      {side.image && <Card.Cover source={{uri: getImageUri(side.image)}} style={{marginBottom: 20}} />}
      <Card.Content>
        <TextInput
          label={t('edit.text', {defaultValue: 'Text'})}
          value={side.text}
          onChangeText={text => onChange({...side, text})}
          multiline
        />
      </Card.Content>
      <Card.Actions>
        <Button icon="camera" disabled={loading} onPress={handleTakePhoto}>
          <Trans i18nKey="edit.camera">Camera</Trans>
        </Button>
        <Button icon="image" disabled={loading} onPress={handlePictureSelect}>
          <Trans i18nKey="edit.picture">Picture</Trans>
        </Button>
        {side.image && (
          <Button icon="delete" onPress={handlePictureDelete} color={Colors.red400}>
            <Trans i18nKey="delete">Delete</Trans>
          </Button>
        )}
      </Card.Actions>
    </Card>
  );
}

import React, {createContext, useCallback, useEffect, useState} from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {cancelable} from 'cancelable-promise/dist/CancelablePromise';

interface Settings {
  darkMode: boolean;
}

const defaultSettings: Settings = {
  darkMode: true
};

interface SettingsContextType extends Settings {
  setDarkMode: (darkMode: boolean) => void;
}

export const SettingsContext = createContext<SettingsContextType>({
  ...defaultSettings,
  setDarkMode: () => {}
});

export const SettingsProvider: React.FC = ({children}) => {
  const [darkMode, setDarkMode_] = useState(false);

  useEffect(() => {
    const promise = cancelable(AsyncStorage.getItem('settings'));

    promise
      .catch(() => JSON.stringify(defaultSettings))
      .then(result => {
        if (!result) {
          return defaultSettings;
        }
        let settings = defaultSettings;
        try {
          settings = JSON.parse(result) as Settings;
        } catch {}
        setDarkMode_(settings.darkMode);
      });

    return () => promise.cancel();
  }, []);

  const setDarkMode = useCallback(async (darkMode: boolean) => {
    setDarkMode_(darkMode);
    await AsyncStorage.setItem('settings', JSON.stringify({darkMode}));
  }, []);

  return <SettingsContext.Provider value={{darkMode, setDarkMode}}>{children}</SettingsContext.Provider>;
};

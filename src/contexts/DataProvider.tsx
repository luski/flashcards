import React, {createContext, useCallback, useEffect, useState} from 'react';

import * as db from '../utils/db';
import {Flashcard, FlashcardSides, Group} from '../types';

type Buckets = [Flashcard[], Flashcard[], Flashcard[], Flashcard[], Flashcard[], Flashcard[]];

interface DataContextType {
  // data:
  flashcardBuckets: Buckets;
  groups: Group[];

  // mutations:
  addFlashcard: (sides: FlashcardSides, groupId: string) => Promise<Flashcard>;
  deleteFlashcard: (flashcardId: string, groupId: string) => Promise<void>;
  updateFlashcard: (flashcardId: string, groupId: string, sides: FlashcardSides) => Promise<void>;
  changeFlashcardGroup: (flashcardId: string, oldGroupId: string, newGroupId: string) => Promise<void>;
  createGroup: (name: string) => Promise<Group>;
  deleteGroup: (groupId: string) => Promise<void>;
  renameGroup: (groupId: string, name: string) => Promise<void>;
  toggleGroup: (groupId: string) => Promise<void>;
  updateFlashcardStars: (flashcardId: string, groupId: string, starsUpdate: StarsUpdate) => Promise<void>;
}

export const DataContext = createContext<DataContextType>({
  flashcardBuckets: [[], [], [], [], [], []],
  groups: [],
  addFlashcard: () =>
    Promise.resolve({
      id: '',
      frontSide: {text: ''},
      backSide: {text: ''},
      stars: 0,
      time: 0,
      groupId: ''
    }),
  deleteFlashcard: () => Promise.resolve(),
  updateFlashcard: () => Promise.resolve(),
  changeFlashcardGroup: () => Promise.resolve(),
  createGroup: () => Promise.resolve({id: '', name: '', count: 0, disabled: false}),
  deleteGroup: () => Promise.resolve(),
  renameGroup: () => Promise.resolve(),
  toggleGroup: () => Promise.resolve(),
  updateFlashcardStars: () => Promise.resolve()
});

export enum StarsUpdate {
  UP,
  DOWN,
  SAME
}

export const DataProvider: React.FC = ({children}) => {
  const [flashcardBuckets, setFlashcardBuckets] = useState<Buckets>([[], [], [], [], [], []]);
  const [groups, setGroups] = useState<Group[]>([]);

  useEffect(() => {
    (async () => {
      await db.initialize();
      const groups = await db.getGroups();
      const flashcards = (await Promise.all(groups.map(({id}) => db.getFlashcards(id)))).flat();
      setGroups(groups);
      setFlashcardBuckets(buckets => buckets.map((b, id) => flashcards.filter(({stars}) => stars === id)) as Buckets);
    })();
  }, []);

  const updateGroup = useCallback((groupId: string, modifier: (group: Group) => Group) => {
    setGroups(groups => groups.map(group => (group.id === groupId ? modifier(group) : group)));
  }, []);

  const addFlashcard = useCallback(
    async (sides: FlashcardSides, groupId: string) => {
      const flashcard = await db.addFlashcard(sides, groupId);
      setFlashcardBuckets(([firstBucket, ...rest]) => [[...firstBucket, flashcard], ...rest]);
      updateGroup(groupId, g => ({...g, count: g.count + 1}));
      return flashcard;
    },
    [updateGroup]
  );

  const deleteFlashcard = useCallback(
    async (flashcardId: string, groupId: string) => {
      await db.deleteFlashcard(flashcardId, groupId);
      setFlashcardBuckets(buckets => buckets.map(bucket => bucket.filter(({id}) => id !== flashcardId)) as Buckets);
      updateGroup(groupId, g => ({...g, count: g.count - 1}));
    },
    [updateGroup]
  );

  const updateFlashcard = useCallback(async (flashcardId: string, groupId: string, sides: FlashcardSides) => {
    await db.updateFlashcard(flashcardId, groupId, sides);
    setFlashcardBuckets(
      buckets => buckets.map(bucket => bucket.map(f => (f.id !== flashcardId ? f : {...f, ...sides}))) as Buckets
    );
  }, []);

  const changeFlashcardGroup = useCallback(
    async (flashcardId: string, oldGroupId: string, newGroupId: string) => {
      await db.changeGroup(flashcardId, oldGroupId, newGroupId);
      updateGroup(oldGroupId, g => ({...g, count: g.count - 1}));
      updateGroup(newGroupId, g => ({...g, count: g.count + 1}));
    },
    [updateGroup]
  );

  const createGroup = useCallback(async (name: string): Promise<Group> => {
    const group = await db.createGroup(name);
    setGroups(groups => [...groups, group]);
    return group;
  }, []);

  const deleteGroup = useCallback(async (groupId: string) => {
    const flashcardsToDelete = await db.getFlashcards(groupId);
    await db.deleteGroup(groupId);
    setGroups(groups => groups.filter(({id}) => id !== groupId));
    setFlashcardBuckets(
      buckets => buckets.map(bucket => bucket.filter(({id}) => flashcardsToDelete.every(f => f.id !== id))) as Buckets
    );
  }, []);

  const toggleGroup = useCallback(async (groupId: string) => {
    await db.toggleGroup(groupId);
    setGroups(groups => groups.map(g => (groupId === g.id ? {...g, disabled: !g.disabled} : g)));
  }, []);

  const renameGroup = useCallback(
    async (groupId: string, name: string) => {
      await db.renameGroup(groupId, name);
      updateGroup(groupId, g => ({...g, name}));
    },
    [updateGroup]
  );

  const updateFlashcardStars = useCallback(
    async (flashcardId: string, groupId: string, starsUpdate: StarsUpdate) => {
      const updateStarsModifier = (count: number) => {
        switch (starsUpdate) {
          case StarsUpdate.SAME:
            return count;
          case StarsUpdate.UP:
            return Math.min(5, count + 1);
          case StarsUpdate.DOWN:
            return Math.max(0, count - 1);
        }
      };
      await db.updateFlashcardStars(flashcardId, groupId, updateStarsModifier);
      const previousBucketNumber = flashcardBuckets.findIndex(bucket => bucket.some(({id}) => id === flashcardId));
      const newBucketNumber = updateStarsModifier(previousBucketNumber);

      const flashcard = flashcardBuckets[previousBucketNumber].find(f => f.id === flashcardId);
      if (!flashcard) {
        throw new Error('Flashcard does not exist');
      }
      const newFlashcard = {...flashcard, time: Date.now(), stars: updateStarsModifier(flashcard.stars)};
      let newFlashcardBuckets;
      if (previousBucketNumber === newBucketNumber) {
        newFlashcardBuckets = flashcardBuckets.map((bucket, num) =>
          num === previousBucketNumber ? bucket.map(f => (f.id === flashcardId ? newFlashcard : f)) : bucket
        ) as Buckets;
      } else {
        newFlashcardBuckets = flashcardBuckets.map((bucket, num) => {
          if (num === previousBucketNumber) {
            return bucket.filter(({id}) => id !== flashcardId);
          }
          if (num === newBucketNumber) {
            return [...bucket, newFlashcard];
          }
          return bucket;
        }) as Buckets;
      }
      setFlashcardBuckets(newFlashcardBuckets);
    },
    [flashcardBuckets]
  );

  return (
    <DataContext.Provider
      value={{
        flashcardBuckets,
        groups,
        addFlashcard,
        deleteFlashcard,
        updateFlashcard,
        changeFlashcardGroup,
        createGroup,
        deleteGroup,
        renameGroup,
        toggleGroup,
        updateFlashcardStars
      }}>
      {children}
    </DataContext.Provider>
  );
};

import React, {useContext} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {StatusBar} from 'react-native';
import {useTranslation} from 'react-i18next';

import GroupsScreen from './screens/GroupsScreen';
import EditFlashcardScreen from './screens/edit/EditFlashcardScreen';
import HomeScreenRouter from './screens/home/HomeScreenRouter';
import {SettingsContext} from './contexts/SettingsProvider';
import {getNativeTheme} from './theme';
import {SideContent} from './types';

export type ParamsList = {
  NoArgs: undefined;
  Edit: {
    initialValues?: {
      flashcardId: string;
      frontSide: SideContent;
      backSide: SideContent;
      groupId: string;
    };
  };
};

const Stack = createStackNavigator();

export default function Router() {
  const {darkMode} = useContext(SettingsContext);
  const theme = getNativeTheme(darkMode);
  const {t} = useTranslation();

  return (
    <NavigationContainer theme={theme}>
      <StatusBar backgroundColor={theme.colors.background} barStyle={theme.dark ? 'light-content' : 'dark-content'} />
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreenRouter} options={{title: t('home.title')}} />
        <Stack.Screen name="Groups" component={GroupsScreen} options={{title: t('groups.title')}} />
        <Stack.Screen name="Edit" component={EditFlashcardScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

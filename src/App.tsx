import 'react-native-gesture-handler';

import React from 'react';
import {Provider as PaperProvider} from 'react-native-paper';
import {RootSiblingParent} from 'react-native-root-siblings';

import Router from './Router';
import {DataProvider} from './contexts/DataProvider';
import {SettingsContext, SettingsProvider} from './contexts/SettingsProvider';
import {getTheme} from './theme';
import './i18n';

export default function App() {
  return (
    <SettingsProvider>
      <SettingsContext.Consumer>
        {({darkMode}) => (
          <PaperProvider theme={getTheme(darkMode)}>
            <DataProvider>
              <RootSiblingParent>
                <Router />
              </RootSiblingParent>
            </DataProvider>
          </PaperProvider>
        )}
      </SettingsContext.Consumer>
    </SettingsProvider>
  );
}

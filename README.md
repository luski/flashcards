# Flashcards

## How to run

1. Install expo on your Android phone (can be found on Google Play)
2. clone this repo
3. `yarn install`
4. `yarn start`
5. Run expo on your phone and scan the QR code that you can see after `yarn start`
